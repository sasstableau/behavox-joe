import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'shortenText'
})
export class ShortenTextPipe implements PipeTransform {

  transform(value: string, length: number, searchString?: string): string {
    const indexOfHighlightedText = value.search(new RegExp(searchString, 'i'));
    if (value.length > length) {
      if (searchString) {
        if (indexOfHighlightedText + 1 < length && length < indexOfHighlightedText + searchString.length + 1) {
          return value.slice(0, indexOfHighlightedText + searchString.length) + '...';
        }
        if (length < indexOfHighlightedText + 1) {
          return value.slice(0, length) + '<span class="highlight">...</span>';
        }
      }
      return value.slice(0, length) + '...';
    }
    return value;
  }
}
