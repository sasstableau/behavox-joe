import {ShortenTextPipe} from './shorten-text.pipe';

const testSentence = 'this is a test sentence with searchable text';
const testSentenceResult1 = 'this is a t...';
const testSentenceResult2 = 'this is a t<span class="highlight">...</span>';
const testSentenceResult3 = 'this is a test...';
describe('ShortenTextPipe', () => {
  it('create an instance', () => {
    const pipe = new ShortenTextPipe();
    expect(pipe).toBeTruthy();
  });

  it('should shorten a test sentence properly', () => {
    const pipe = new ShortenTextPipe();
    expect(pipe.transform(testSentence, 11)).toBe(testSentenceResult1);
  });

  it('should shorten a test sentence properly when search result is beyond length', () => {
    const pipe = new ShortenTextPipe();
    expect(pipe.transform(testSentence, 11, 'text')).toBe(testSentenceResult2);
  });

  it('should shorten a test sentence properly when search is by crossing the end point', () => {
    const pipe = new ShortenTextPipe();
    expect(pipe.transform(testSentence, 12, 'test')).toBe(testSentenceResult3);
  });

});
