import {JoinArrayPipe} from './join-array.pipe';

describe('JoinArrayPipe', () => {
  it('create an instance', () => {
    const pipe = new JoinArrayPipe();
    expect(pipe).toBeTruthy();
  });

  it('should string arrays properly', () => {
    const pipe = new JoinArrayPipe();
    expect(pipe.transform(['test', 'test2'])).toBe('test, test2');
  });
});
