import {SafeHtmlPipe} from './safe-html.pipe';
import {DomSanitizer, SafeHtml} from '@angular/platform-browser';
import {Inject} from '@angular/core';

describe('SafeHtmlPipe', () => {
  it('create an instance', () => {
    const pipe = new SafeHtmlPipe(Inject(DomSanitizer));
    expect(pipe).toBeTruthy();
  });
});
