import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'searchTextHighlight'
})
export class SearchTextHighlightPipe implements PipeTransform {

  transform(value: string, searchText: string) {
    if (searchText?.trim()) {
      const reg = new RegExp(searchText + '(?![^<]*>)', 'gi');
      return value.replace(reg, (match: string) => `<span class="highlight">${match}</span>`);
    }
    return value;
  }
}
