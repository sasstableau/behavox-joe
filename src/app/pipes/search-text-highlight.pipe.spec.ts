import {SearchTextHighlightPipe} from './search-text-highlight.pipe';

describe('SearchTextHighlightPipe', () => {
  it('create an instance', () => {
    const pipe = new SearchTextHighlightPipe();
    expect(pipe).toBeTruthy();
  });

  it('should highlight a searched text', () => {
    const pipe = new SearchTextHighlightPipe();
    expect(pipe.transform('test', 'test')).toBe('<span class="highlight">test</span>');
  });

});
