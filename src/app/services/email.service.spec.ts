import {TestBed} from '@angular/core/testing';
import {EmailService} from './email.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {mockFilters} from './email.model';

describe('EmailService', () => {
  let service: EmailService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ]
    });
    service = TestBed.inject(EmailService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should get the latest date', () => {
    service.getLatestEmailDate().subscribe((date) => {
      expect(date.toLocaleDateString()).toBe('1/1/2002');
    });
  });

  it('#contacts should contain k..allen@enron.com', () => {
    service.getContacts().subscribe((contacts) => {
      expect(contacts).toContain('k..allen@enron.com');
    });
  });

  it('#should filter according to joe', () => {
    service.getEmailsByFilters(mockFilters()).subscribe((emails) => {
      expect(emails.length).toBe(2);
    });
  });
});
