export interface Email {
  id: string;
  from: string;
  to: string[];
  bcc: string[];
  cc: string[];
  subject: string;
  body: string;
  date: string;
  threads?: string[];
}

export interface Filters {
  textSearch?: string;
  dateFilter?: string;
  contactsFilter?: string;
}

export const mockFilters = () => ({
  textSearch: 'joe'
});

export const mockEmail = () => ({
  id: 'arsystem@mailman.enron.comk..allen@enron.com2001-10-30T00:22:13.000+0000',
  from: 'arsystem@mailman.enron.com',
  to: [
    'k..allen@enron.com'
  ],
  cc: [],
  bcc: [],
  subject: 'Your Approval is Overdue: Access Request for matt.smith@enron.com',
  body: 'test ---originial---- forwarded',
  date: '2001-10-30T00:22:13.000+0000'
});
