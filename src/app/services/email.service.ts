import {Injectable} from '@angular/core';
import {Email, Filters} from './email.model';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {map, shareReplay} from 'rxjs/operators';
import {deburr, filter, reduce, toLower, uniq, map as _map} from 'lodash';

@Injectable({
  providedIn: 'root'
})
export class EmailService {

  constructor(private http: HttpClient) {}

  public getContacts(): Observable<string[]> {
    return this._getMailJson()
      .pipe(
        map(emails => uniq(reduce(emails, (c, v) => {
          return [...c, v.from, ...v.to];
        }, []))),
        shareReplay(1)
      );
  }

  public getLatestEmailDate(): Observable<Date> {
    return this._getMailJson()
      .pipe(
        map(emails => reduce(emails, (c, v) => {
          const currentDate = new Date(v.date);
          const accDate = new Date(c);
          return currentDate.getTime() > accDate.getTime() ? currentDate : accDate;
        }, new Date('01/01/01')))
      );
  }

  public getEmailsByFilters({textSearch, dateFilter, contactsFilter}: Filters): Observable<Email[]> {
    return this._getMailJson()
      .pipe(
        map(emails => {
          textSearch = deburr(toLower(textSearch));
          if (textSearch) {
            const reg = new RegExp(textSearch + '(?![^<]*>)', 'gi');
            emails = filter(emails, email =>
              deburr(toLower(email.body))?.search(reg) > -1
              || deburr(toLower(email.subject))?.search(reg) > -1
            );
          }
          if (contactsFilter) {
            emails = filter(emails, email => email.from === contactsFilter
              || email.to.indexOf(contactsFilter) > -1);
          }
          if (dateFilter) {
            const date = new Date(dateFilter);
            emails = filter(emails, email => new Date(email.date).toDateString() === date.toDateString());
          }
          return emails;
        })
      );
  }

  private _getMailJson(): Observable<Email[]> {
    return this.http.get<Email[]>('assets/email.json')
      .pipe(
        map(emails => _map(emails, email => ({
          ...email,
          id: email.from + email.to.join('') + email.date
        }))),
        shareReplay(1)
      );
  }
}
