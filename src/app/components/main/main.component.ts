import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {Observable} from 'rxjs';
import {map, shareReplay, switchMap} from 'rxjs/operators';
import {Email, Filters} from '../../services/email.model';
import {MatPaginator} from '@angular/material/paginator';
import {EmailService} from '../../services/email.service';
import {MatTableDataSource} from '@angular/material/table';
import {MatSort} from '@angular/material/sort';
import {animate, state, style, transition, trigger} from '@angular/animations';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ]
})
export class MainComponent implements OnInit {
  @ViewChild(MatPaginator) public paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) public sort: MatSort;
  @Input() filters$: Observable<Filters>;

  public pageSize = 50;
  public displayedColumns = ['from', 'to', 'subject', 'body', 'date'];
  public expandedEmail: Email;

  public highlightedText$: Observable<string>;
  public emails$: Observable<MatTableDataSource<Email>>;

  constructor(
    private _emailService: EmailService
  ) {}

  public ngOnInit() {
    this.highlightedText$ = this.filters$
      .pipe(map(({textSearch}: Filters) => textSearch));
    this.emails$ = this.filters$
      .pipe(
        switchMap(filters => this._emailService.getEmailsByFilters(filters)),
        map(emails => {
          const dataSource = new MatTableDataSource(emails);
          dataSource.paginator = this.paginator;
          dataSource.sort = this.sort;
          return dataSource;
        }),
        shareReplay(1)
      );

  }

  public trackFn(index, item: Email) {
    return item?.id;
  }

  public setExpandedEmail(email: Email) {
    this.expandedEmail = email.id === this.expandedEmail?.id ? null : email;
  }

}
