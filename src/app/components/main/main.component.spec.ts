import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {MainComponent} from './main.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {MatCardModule} from '@angular/material/card';
import {MatTableModule} from '@angular/material/table';
import {MatSortModule} from '@angular/material/sort';
import {MatPaginatorModule} from '@angular/material/paginator';
import {EmailService} from '../../services/email.service';
import {SafeHtmlPipe} from '../../pipes/safe-html.pipe';
import {SearchTextHighlightPipe} from '../../pipes/search-text-highlight.pipe';
import {ShortenTextPipe} from '../../pipes/shorten-text.pipe';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {EmailComponent} from '../email/email.component';
import {of} from 'rxjs';
import {mockEmail, mockFilters} from '../../services/email.model';
import {JoinArrayPipe} from '../../pipes/join-array.pipe';

describe('MainComponent', () => {
  let component: MainComponent;
  let fixture: ComponentFixture<MainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        MatCardModule,
        MatTableModule,
        MatSortModule,
        MatPaginatorModule,
        BrowserAnimationsModule
      ],
      providers: [EmailService],
      declarations: [MainComponent, SafeHtmlPipe, SearchTextHighlightPipe, ShortenTextPipe, EmailComponent, JoinArrayPipe]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainComponent);
    component = fixture.componentInstance;
    component.filters$ = of(mockFilters());
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set expandedEmail', () => {
    component.setExpandedEmail(mockEmail());
    fixture.detectChanges();
    expect(component.expandedEmail.date).toBe(mockEmail().date);
  });

  it('should set emails$ table date right', () => {
    component.emails$.subscribe(emails => {
      expect(emails.data.length).toBe(2);
    });
  });

});
