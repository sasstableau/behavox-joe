import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {EmailComponent} from './email.component';
import {SafeHtmlPipe} from '../../pipes/safe-html.pipe';
import {SearchTextHighlightPipe} from '../../pipes/search-text-highlight.pipe';
import {mockEmail} from '../../services/email.model';
import {JoinArrayPipe} from '../../pipes/join-array.pipe';

describe('EmailComponent', () => {
  let component: EmailComponent;
  let fixture: ComponentFixture<EmailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [EmailComponent, SafeHtmlPipe, SearchTextHighlightPipe, JoinArrayPipe]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmailComponent);
    component = fixture.componentInstance;
    component.email = mockEmail();
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should thread the email properly', () => {
    expect(component.threadedEmail.threads.length).toBeTruthy(1);
  });
});
