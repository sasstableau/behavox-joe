import {Component, Input} from '@angular/core';
import {Email} from '../../services/email.model';
import {split, head, filter as _filter, tail} from 'lodash';

@Component({
  selector: 'app-email',
  templateUrl: './email.component.html',
  styleUrls: ['./email.component.scss']
})
export class EmailComponent {
  @Input() highlightedText: string;

  @Input()
  public set email(email: Email) {
    if (email) {
      this.threadedEmail = this._threadEmail(email);
    }
  }

  public threadedEmail: Email;

  private _threadRegex = /(--[-+].*[-+]--)/gi;

  private _threadEmail(email): Email {
    const emailThreads = _filter(split(email.body, this._threadRegex), part => !part.match(this._threadRegex));
    return {
      ...email,
      body: head(emailThreads),
      threads: tail(emailThreads)
    };

  }

}
