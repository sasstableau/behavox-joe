import {TestBed, async} from '@angular/core/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {AppComponent} from './app.component';
import {MatInputModule} from '@angular/material/input';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {EmailService} from './services/email.service';
import {MatIconModule} from '@angular/material/icon';
import {MatFormFieldModule} from '@angular/material/form-field';
import {ReactiveFormsModule} from '@angular/forms';
import {MatCardModule} from '@angular/material/card';
import {MatNativeDateModule} from '@angular/material/core';
import {MainComponent} from './components/main/main.component';
import {EmailComponent} from './components/email/email.component';
import {MatPaginatorModule} from '@angular/material/paginator';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        MatInputModule,
        MatAutocompleteModule,
        MatDatepickerModule,
        MatIconModule,
        MatFormFieldModule,
        ReactiveFormsModule,
        MatCardModule,
        MatNativeDateModule,
        MatPaginatorModule,
        HttpClientTestingModule
      ],
      providers: [EmailService],
      declarations: [
        AppComponent,
        MainComponent,
        EmailComponent
      ],
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });
});
