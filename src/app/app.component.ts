import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {EmailService} from './services/email.service';
import {BehaviorSubject, combineLatest, Observable, Subject} from 'rxjs';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {filter, map, take, takeUntil} from 'rxjs/operators';
import {isEmpty, filter as _filter, omitBy} from 'lodash';
import {MatDatepicker} from '@angular/material/datepicker';
import {Filters} from './services/email.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
  @ViewChild(MatDatepicker) public picker: MatDatepicker<string>;

  public textSearchControl = new FormControl('');
  public dateFilterControl = new FormControl('');
  public contactsFilterControl = new FormControl('',
    Validators.pattern(/^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/i)
  );
  public filtersFormGroup: FormGroup;
  public datePickerStartDate$: Observable<Date>;
  public contactOptions$: Observable<string[]>;

  public get filters$(): Observable<Filters> {
    return this._filters$.asObservable();
  }

  public _filters$ = new BehaviorSubject<Filters>({});
  private _destroy$ = new Subject();

  constructor(
    private _emailService: EmailService,
    private _fb: FormBuilder,
    private _router: Router,
    private _activatedRoute: ActivatedRoute
  ) {
    this.contactOptions$ = combineLatest([
      this._emailService.getContacts(),
      this.contactsFilterControl.valueChanges
    ]).pipe(
      map(([contacts, contactsFilter]) => _filter(contacts, contact => contact.indexOf(contactsFilter) > -1))
    );
    this.datePickerStartDate$ = this._emailService.getLatestEmailDate();
    this.filtersFormGroup = this._fb.group({
      textSearch: this.textSearchControl,
      dateFilter: this.dateFilterControl,
      contactsFilter: this.contactsFilterControl
    });
  }

  public ngOnInit() {
    this._activatedRoute.queryParams.pipe(
      filter(values => !isEmpty(values)),
      take(1)
    ).subscribe(({textSearch, contactsFilter, dateFilter}: Filters) => {
      this.filtersFormGroup.setValue({
        textSearch: textSearch ?? '',
        contactsFilter: contactsFilter ?? '',
        dateFilter: dateFilter ? new Date(dateFilter) : ''
      });
    });
    this.filtersFormGroup.valueChanges
      .pipe(
        takeUntil(this._destroy$)
      ).subscribe(({textSearch, contactsFilter, dateFilter}: Filters) => {
      const cleanedFilters = omitBy({
        textSearch: textSearch ? textSearch.trim() : '',
        contactsFilter: this.contactsFilterControl.valid && contactsFilter ? contactsFilter.trim() : '',
        dateFilter: dateFilter ? new Date(dateFilter).toLocaleDateString() : ''
      }, value => !value);
      this._filters$.next(cleanedFilters);
      this._router.navigate([], {
        relativeTo: this._activatedRoute,
        queryParams: cleanedFilters
      });
    });
  }

  public ngOnDestroy() {
    this._destroy$.next(true);
    this._destroy$.unsubscribe();
  }

}
